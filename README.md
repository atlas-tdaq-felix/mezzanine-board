## Mezzanine-board

### Description
The Mezzanine-board provides 24 RX/TX channels capable of 25Gb/s using Samtec Firefly modules and SI5345 jitter cleaners on a FMC+ mezzanine board. There is also provisions for White rabbit timing synchronisation.

### Block schematic

![Blockschematic](Blockschematic.png "Blockschematic")

### Shematic
Download the schematic [here.](38660.03.02.0_SCH.PDF):

### Firefly positions

![FF pos](FF_pos.png "FF pos")

### Modifications
There are some modifications needed on the PCB for it to function correctly:
- remove U18
- D1 connect pin 1 to vcc(p8)
- R80 change to 39KOhm
- U17 Swap SCL and SDA of ch0 to ch3

### Pictures

![PCB top](IMG_20211213_091939.jpg "PCB top")
![PCB bottom](IMG_20211213_091952.jpg "PCB bottom")
